# IGT Purposal

## Justification

The idea of the IGT is something that's brewing in my mind since 2017, or earlier. It's meant to provide another option for consumers to buy video game tokens such as PLEX, WoW Tokens, or 90% of all free-to-play mobile games. Because many people like these games but can't always afford the extra price to play them, especially in the latter case.

The reason we have video game tokens pegged to the dollar to begin with is simple ecomonics. It keeps the economy stable by preventing runaway inflation. Second Life is an early example of this. This mostly works in an MMO setting, such as WoW or EVE, but is often applied to free-to-play games to make up for being free. Such tokens also exist in "AAA" single player games but I've never really understood their justifications.

As mentioned, Second Life was an early example. Their currency, the Linden Dollar, L$, was always pegged to the dollar but you were able to get free L$50 via a stipend everytime you logged in until mid-2006. It was removed for future free accounts so they had to buy new L$ using their exchange. However, accounts made before mid-2006 or subscribers make L$50 to L$2,000, respectfully. This has had the effect of most basic items for your avatar costing under L$50 to L$1,000.

This is all well and good but what if you wanted an luxary item, valued over L$1,000 but didn't have the extra dollars to afford it. Then what? Crypcocurrency is one way to solve this problem. The problem is Bitcoin and others are often a speculative risk. Their value can flux from ultra high to low in a matter of days. Very few cryptocurrencies have values that are stable enough for everyday usage. They're other problems that also factor in why it would be a bad idea but I won't go into this here. Having an application-speififc cryptocurrency, such as IGT, is more practical in this scenerio.

## Cosmos

IGT would be designed using the [Cosmos SDK](https://cosmos.network/). Cosmos is similar to Ethereum in it's purpose but allows for focusing on building public or permissive blockchains based on a common arthicture, [Tendermint](https://tendermint.com), that can effortlessly interop with each other. This is constrast to Ethereum's method of having it's blockchain handle all transactions. The problem with Ethereum's method is when a cryptocurrency on the network clogs the blockchain or something as bad as the failure of an autonomous organization leads to an emergency hard-fork of the entire platform, such as [The DAO](https://medium.com/swlh/the-story-of-the-dao-its-history-and-consequences-71e6a8a551ee).

We do not need this added risk. This is why I Cosmos was choosen. It's still in development, around 80% complete as of this wiriting, and live only on it's testnet.

## Spec

- Ticker: XIG
- Symbol: IGT
- Subunits: mIG, uIG, sIG
- Smallest amount: 0.00000001

## Mining

**SUBJECT TO CHANGE.**

IGT is a permissive blockchain. Coins are generated in a similar manner to Second Life's pre-2006 stipend program. A user starts out with 1 IGT upon initial wallet creation and earns an additional 0.1 LGT the next week based on their usage the previous week. This slowly dwindles overtime until 0.0000001 IGT per week, the emissions peroid.

## Wallets

Wallets may be either full node or lightweight. Both using a deterministic keys for recovery. Addresses are similar to Bitcoin's and are prefixed with IG. Video games are recommended to use lightweight wallets, if they wish to tie into the blockchain directly.

## Scenerios

### SimCity 2013

SimCity is technically a single player but 2013 adds a multiplayer element for exchanging raw matrial and items. Money in all SimCity games is calcuated in the thousands since the focus is on infrastructure, as oppose to everyday items found in The Sims. Adding in the IGT could be useful for stabilziing the online economy as well as providing a method of purchasing in-game items.

One scenerio is 1 IGT being valued at the game's most expensive item or commodity. This way all other items can be purchased in the decimal range. For example, §15,000 Fire Station would cost 0.005 IGT while the §25,000 Academy would cost 0.02 IGT.