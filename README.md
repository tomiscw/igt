![Inter-game Token](banner.png)

The IgT, Inter-game Token, is a exchange token that will be based on the Cosmos SDK. It's meant to act as a peg for video game tokens, such as EVE's Plex or the WoW Token.